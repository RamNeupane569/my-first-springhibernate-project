package com.cerotid.my_first_springhibernate_project;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {

		Alien alien = new Alien();
		Configuration con = new Configuration().configure().addAnnotatedClass(Alien.class);

		ServiceRegistry sr = new StandardServiceRegistryBuilder().applySettings(con.getProperties()).build();

		SessionFactory sf = con.buildSessionFactory(sr);

		Session session = sf.openSession();

		Transaction tx = session.beginTransaction();

		alien = (Alien) session.get(Alien.class, 102);

		tx.commit();

		System.out.println(alien);
	}
}
